Welcome to the NOV MAD Upgrade team technical interview coding challenge!

You will be tested on 2 main things: 
	1. Your understanding of the angular architecture/paradigm 
	2. Your ability to pick things up quickly

This is not a pass/fail challenge. The purpose is for us to get an idea for how much angular you know and to discuss higher level
architecture. You are not expected to finish all of the challenge as the time you have is very short.

To prepare your environment for the challenge follow the steps below:

1. Make sure NodeJS and GIT are installed on your machine. 

2. Open terminal and run npm install http-server -g 

3. Navigate to the root directory for this project and run http-server

4. Open browser and navigate to one of the urls displayed in terminal, one of them will likely be: http://127.0.0.1:8080/ 
When you make changes in the project, refresh this page to see the changes reflected

5. In browser navigate to https://madlibs-85bba.firebaseio.com/ this is the NOSQL database you will be using as your back end in the challenge. 

THE CHALLENGE:

You have a limited amount of time, likely about 30 minutes or less. The challenge has 2 main sections with 1 bonus section, 
try to follow the order outlined but know you will likely not have time to accomplish both sections and feel free to move on to 
the next section if you get stuck. Also feel free to ask any questions to any of the team members during the test, this allows us to 
get an idea of what it feels like to work together! 

PART ONE:

Your task is to create angular components and separate out the logic in the best way you can in the time you have. Feel free to reference the 
angularjs style guide (https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md).

The app you have cloned is Mad Libs. Mad Libs is a phrasal template word game where one player prompts others for a list of words to substitute 
for blanks in a story, before reading the – often comical or nonsensical – story aloud. The game is frequently played as a party game or as a 
pastime. 

As you can see all of the logic for this mad lib app is in one controller and in one file. Even though this is a small project, it might be ideal
to separate the logic out into different angular components. Create those components and separate out the logic in the best way you can in the 
time you have. You should not have to write very many new lines of code, but instead should be cutting and pasting code, to help this app adhere 
more closely to the angular way. 

PART TWO:

Your task is to add saving functionality to app.

The likelihood of two stories of ever being the same is very low. However, in the current state of the project the app simply reads from the
FirebaseDB. We would like to write or save each story generated to the firebaseDB and then display them in the app. 

BONUS:

Impress us with your Angular expertise by making a simple directive. Any custom directive will be impressive. (Hint: perhaps a directive that
is responsible for displaying a historical madlib story)

DOUBLE BONUS:

Implement simple routing functionality.

