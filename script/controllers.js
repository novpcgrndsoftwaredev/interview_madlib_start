(function(){
	angular.module('app')
	.controller('madlibsController', madlibsCtrl);

	// controllers' purpose is to facilitate communication between the view (html) and the model (the data)
	function madlibsCtrl($q){
		var _stories, _nouns, _adjectives, _adverbs, _verbs;
		var vm = this;
		// Bound functions
		vm.generateStory = generateStory;

		// Bound variables
		vm.words = {
			nouns: '',
			verbs: '',
			adjectives: '',
			adverbs: ''
		};
		vm.storyName = 'Choose a Story';
		vm.storyText = '^^^ Do the thing!!! ^^^';
		vm.currStory = '';
		vm.stories = [];
		initData().then(function (data) {
			vm.stories = getStories();
		});

		function generateStory(){
			vm.storyName = vm.currStory;
			vm.storyText = createStory(vm.storyName,vm.words);
		}

		function injectWords(story,words){
			for(var key in words){
				if(words.hasOwnProperty(key)){
					for(var i = 0, target; i < words[key].length; i++){
						target = '#' + key;
						story = story.replace(target, words[key][i]);
					}
				}
			}
			return story;
		}

		function shuffle(arr) {
			var currIndex = arr.length, temp, randIndex ;
			// While there remain elements to shuffle...
			while (0 !== currIndex) {
				// Pick a remaining element...
				randIndex = Math.floor(Math.random() * currIndex);
				currIndex -= 1;
				// And swap it with the current element.
				temp = arr[currIndex];
				arr[currIndex] = arr[randIndex];
				arr[randIndex] = temp;
			}
			return arr;
		}
		function combineWords(inputWords,defaultWords){
			var words = {
				nouns: new Array(4),
				verbs: new Array(4),
				adjectives: new Array(4),
				adverbs: new Array(4)
			};
			for(var key in defaultWords){
				if(defaultWords.hasOwnProperty(key)){
					shuffle(defaultWords[key]);
				}
			}
			for(var type in words){
				if(words.hasOwnProperty(type)){
					for (var i = 0; i < words[type].length; i++) {
						words[type][i] = (inputWords[type][i] !== undefined) ? inputWords[type][i] : defaultWords[type][i];
					}
				}
			}
			return words;
		}

		function stringObjToArrObj(obj){
			for(var key in obj){
				if(obj.hasOwnProperty(key) && typeof obj[key] !== 'object'){
					obj[key] =  obj[key].length > 0 ? obj[key].split(',') : [];
				}
			}
			return obj;
		}

		function createStory(name,input){
			var story = getStory(name);
			var defaultWords = getDefaultWords();
			var inputWords = stringObjToArrObj(input);
			var words = combineWords(inputWords,defaultWords);
			story = injectWords(story,words);
			return story;
		}
		
		function initData() {
			return $q.all([getStoriesFromDb(), getVerbs(), getNouns(), getAdverbs(), getAdjectives()]).then(function(data){
				console.log(data);
				_stories = data[0];
				_verbs = data[1];
				_nouns = data[2];
				_adverbs = data[3];
				_adjectives = data[4];
				return data;
			});
		}
		function getStory(name){
			return _stories[name || 'shopping'];
		}

		function getStories(){
			return Object.keys(_stories);
		}

		function getDefaultWords(){
			return {
				nouns: _nouns,
				verbs: _verbs,
				adjectives: _adjectives,
				adverbs: _adverbs
			};
		}
		function getVerbs() {
			return firebase.database().ref('verbs').once('value').then(function(snapshot){
				return snapshot.val();
			});
		}
		function getNouns() {
			return firebase.database().ref('nouns').once('value').then(function(snapshot){
				return snapshot.val();
			});
		}
		function getAdverbs() {
			return firebase.database().ref('adverbs').once('value').then(function(snapshot){
				return snapshot.val();
			});
		}
		function getAdjectives() {
			return firebase.database().ref('adjectives').once('value').then(function(snapshot){
				return snapshot.val();
			});
		}
		function getStoriesFromDb() {
			return firebase.database().ref('stories').once('value').then(function(snapshot){
				return snapshot.val();
			});
		}
	}
})();
